@extends('layouts.app')

@section('content')
    <h1>Table resevation confirmation</h1>

    <ul>
        <li>Name: {{ session('cart.reservation_info.name') }}</li>
        <li>Email: {{ session('cart.reservation_info.email') }}</li>
        <li>Kontaktinis telefonas: {{ session('cart.reservation_info.contact_phone') }}</li>
        <li>Asmenu skaicius: {{ session('cart.reservation_info.number_of_persons') }}</li>
        <li>Staliukas: {{ $table->title }}</li>
        <li>Data: {{ session('cart.reservation_info.reservation_date') }}</li>
        <li>Laikas: {{ session('cart.reservation_info.reservation_time') }}</li>
    </ul>

    {!! Form::open(['route' => ['orders.store'], 'method' => 'post'])!!}
        {!! Form::submit('Confirm' , ['class' => 'btn btn-success'])!!}
    {!! Form::close() !!}
@endsection
