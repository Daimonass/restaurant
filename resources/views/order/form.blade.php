@extends('layouts.app')

@section('content')

  @if(isset($order))
    {!! Form::model($order, ['route' => ['orders.update', $order->id], 'method' => 'put', 'files' => true])  !!}
  @else
    {!! Form::open(['route' => ['orders.store'], 'method' => 'post', 'files' => true])  !!}
  @endif

<div class="form-group">
  {!! Form::label('name', 'Vardas'); !!}
	{!!  Form::text('name', isset($order) ? $order->name : Auth::user()->name, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
</div>

<div class="form-group">
  {!! Form::label('email', 'Email'); !!}
	{!!  Form::text('email', isset($order) ? $order->email : Auth::user()->email, ['class' => 'form-control', 'placeholder' => 'Emailas']) !!}
</div>

<div class="form-group">
  {!! Form::label('number_of_persons', 'Number of persons'); !!}
	{!!  Form::text('number_of_persons', null, ['class' => 'form-control', 'placeholder' => 'Number of persons']) !!}
</div>

<div class="form-group">
  {!! Form::label('reservation_date', 'Reservation date'); !!}
	{!!  Form::text('reservation_date', null, ['class' => 'form-control', 'placeholder' => 'reservation_date']) !!}
</div>

<div class="form-group">
  {!! Form::label('reservation_time', 'Reservation time'); !!}
	{!!  Form::text('reservation_time', null, ['class' => 'form-control', 'placeholder' => 'reservation_time']) !!}
</div>

{{-- <div class="form-group">
  {!! Form::label('table_id', 'Select table'); !!}
	{!!  Form::select('table_id', $table, null, ['class' => 'form-control', 'placeholder' => 'reservation_time']) !!}
</div> --}}

{!! Form::submit('Update',['class' => 'btn btn-warning']) !!}

{{ Form::close() }}
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<table class="table">
              <thead>
                <tr>
                  <th>Photo</th>
                  <th>Order id</th>
                  <th>Table name  </th>
                  <th>Dishes count</th>
                  <th>Price</th>
                  <th></th>
                </tr>
              </thead>

              @if(count($order->order_lines()) > 0)
              <tbody>
                @foreach($order->order_lines as $item)
                <tr>
                  <td><img style="max-width: 50px;" src="{{ asset('/storage/' . $item->dish->photo) }}" alt=""> {{ $item->dish->title }}</td>
                  <td> {{ $order->id }} </td>
                  <td> {{ $order->table->title }} </td>
                  <td> {{ $item->quantity }}</td>
                  <td> {{ $item ->total }}€</td>
          					<td>
          						{!! Form::open(['route' => ['orders.line_destroy', $item->id], 'method' => 'delete'])!!}
          							{!! Form::submit('X' , ['class' => 'btn btn-danger'])!!}
          						{!! Form::close() !!}
          					</td>
          				</tr>
          			@endforeach
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="4" class="text-right">Total price:</td>
                  {{-- <td>{{ $order->total }}</td> --}}
                  <td>{{ $order->order_lines->sum('total') }}€</td>
                </tr>
              </tfoot>
              @endif

            </table>


    {!! Form::open(['route' => ['add.dist_to_order', $order->id] 'method' => 'PUT'])  !!}

    <div class="form-group">
      {!! Form::label('dishes', 'Select dishes'); !!}

      <select class="btn btn-primary dropdown-toggle" name="dishes">
        @foreach ($dishes as $dish)
          <option value="{{$dish->id}}">{{$dish->title}}</option>
        @endforeach
      </select>

      <!-- Single button -->
{{-- <div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Select dishes <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    @foreach ($dishes as $dish)
    <li name="{{$dish->id}}"><a href="#">{{$dish->title}}</a></li>
    @endforeach
  </ul>
</div> --}}
    </div>

    {!! Form::submit('Add dish',['class' => 'btn btn-primary']) !!}

    {{ Form::close() }}










@endsection
