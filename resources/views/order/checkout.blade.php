@extends('layouts.app')

@section('content')

  @if(session('cart.items') && count(session('cart.items')) > 0)
        <table class="table table-bordered text-center">
          <thead>
            <tr>
              <th>Photo</th>
              <th>Pavadinimas</th>
              <th>Kiekis</th>
              <th>Vnt. Kaina</th>
              <th>PVM</th>
              <th>Viso be PVM</th>
              <th>Viso</th>
            </tr>
          </thead>
          <tbody>
            @foreach(session('cart.items') as $item)
            <tr>
              <td><img src="{{ asset('/storage/' . $item['photo']) }}" alt="{{ $item['title'] }}" class="img-responsive center-block cartImage" style="width: 200px"></td>
                <td><span class="cartImage">{{ $item['title']}}</span></td>
              </td>
              <td>{{ $item['quantity']}}</td>
              <td>{{ $item['price']}}</td>
              <td>{{ $item['vat'] }}&euro;</td>
              <td>{{ $item['without_vat'] }}&euro;</td>
              <td>{{ $item['total']}} &euro;</td>
              <td><a href="{{ route('cart.delete_line', ['id' => $item['id']]) }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ session('cart.total_vat') ?: '0' }} &euro;</td>
            <td>{{ session('cart.total_without_vat') ?: '0' }} &euro;</td>
            <td> {{ session('cart.total') ?: '0' }} &euro;</td>
          </tfoot>

        </table>

<h1>Reservation info</h1>
{!! Form::open(['route' => 'checkout.store', 'method' => 'post']) !!}

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

  {!! Form::label('name', 'Contact Name'); !!}
  {!! Form::text('name', Auth::user() ? Auth::user()->name : null , ['class' => 'form-control', 'placeholder' => 'Name']) !!}

  @if ($errors->has('name'))
  <span class="help-block">
      <strong>{{ $errors->first('name') }}</strong>
  </span>
  @endif
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
  {!! Form::label('email', 'Email'); !!}
	{!!  Form::text('email', Auth::user() ? Auth::user()->email : null, ['class' => 'form-control', 'placeholder' => 'Emailas']) !!}

  @if ($errors->has('email'))
  <span class="help-block">
      <strong>{{ $errors->first('email') }}</strong>
  </span>
  @endif
</div>

<div class="form-group{{ $errors->has('contact_phone') ? ' has-error' : '' }}">
  {!! Form::label('contact_phone', 'Contact phone number'); !!}
  {!!  Form::text('contact_phone', Auth::user() ? Auth::user()->phone : null, ['class' => 'form-control', 'placeholder' => 'number']) !!}
  @if ($errors->has('contact_phone'))
  <span class="help-block">
      <strong>{{ $errors->first('contact_phone') }}</strong>
  </span>
  @endif
</div>

<div class="form-group{{ $errors->has('number_of_persons') ? ' has-error' : '' }}">
  {!! Form::label('number_of_persons', 'Person Number'); !!}
  {!! Form::number('number_of_persons', 1 ,['class' => 'form-control','min' => 1,'max' => 10]) ; !!}
  @if ($errors->has('number_of_persons'))
  <span class="help-block">
      <strong>{{ $errors->first('number_of_persons') }}</strong>
  </span>
  @endif
</div>

<div class="form-group{{ $errors->has('table_id') ? ' has-error' : '' }}">
  {!! Form::label('table_id', 'Select table'); !!}
  {!! Form::select('table_id', $tables, null, ['class' => 'form-control']) !!}
  @if ($errors->has('table_id'))
  <span class="help-block">
      <strong>{{ $errors->first('table_id') }}</strong>
  </span>
  @endif
</div>

<div class="form-group{{ $errors->has('reservation_date') ? ' has-error' : '' }}">
  {!! Form::label('reservation_date', 'Reservation date'); !!}
  {!!  Form::date('reservation_date', Carbon\Carbon::now(), ['class' => 'form-control']) !!}
  @if ($errors->has('reservation_date'))
  <span class="help-block">
      <strong>{{ $errors->first('name') }}</strong>
  </span>
  @endif
</div>

<div class="form-group{{ $errors->has('reservation_time') ? ' has-error' : '' }}">
  {!! Form::label('reservation_time', 'Reservation time'); !!}
  {!!  Form::time('reservation_time', Carbon\Carbon::now()->toTimeString(), ['class' => 'form-control']) !!}
  @if ($errors->has('reservation_time'))
  <span class="help-block">
      <strong>{{ $errors->first('reservation_time') }}</strong>
  </span>
  @endif
</div>

    {!! Form::submit('Sukurti rezervacija',['class' => 'btn btn-warning pull-right']) !!}

{!! Form::close() !!}

        {{-- <a href="{{ route('orders.create') }}" class="btn btn-danger pull-right">Sukurti rezervacija</a> --}}
        {{-- <a href="{{ route('saveToSession') }}" class="btn btn-warning pull-right">Sukurti rezervacija</a> --}}
@else
  <h1><center>Cart is empty</center></h1>

@endif
@endsection
