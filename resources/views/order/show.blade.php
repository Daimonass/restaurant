  @extends('layouts.app')

@section('content')




  <h1>{{$order->name}}</h1>




  <p>Email:{{ $order->email}}</p>


  <ul>
  	<li>Total: {{ $order->total }}€ </li>
    <li>Date: {{ $order->date }}€ </li>
  </ul>


  <br>
  <table class="table">
  							<thead>
  								<tr>
                    <th>Photo</th>
                    <th>Order id</th>
                    <th>Table name  </th>
                    <th>Phone</th>
                    <th>Dishes count</th>
                    <th>Email</th>
                    <th>Reservation date</th>
                    <th>Reservation time</th>
                    <th>Price</th>
  								</tr>
  							</thead>

  							@if(count($order->order_lines()) > 0)
  							<tbody>
  								@foreach($order->order_lines as $item)
  								<tr>
  									<td><img style="max-width: 50px;" src="{{ asset('/storage/' . $item->dish->photo) }}" alt=""> {{ $item->dish->title }}</td>
                    <td> {{ $order->id }} </td>
                    <td> {{ $order->table->title }} </td>
                    <td> {{ $order->contact_phone }} </td>
                    <td> {{ $item->quantity }}</td>
                    <td> {{ $order->email }} </td>
                    <td> {{ $order->reservation_date }} </td>
                    <td> {{ $order->reservation_time }} </td>
                    <td> {{ $item ->total }}€</td>
  								</tr>
  								@endforeach
  							</tbody>
  							<tfoot>
  								<tr>
  									<td colspan="8" class="text-right">Total price:</td>
  									{{-- <td>{{ $order->total }}</td> --}}
  									<td>{{ $order->order_lines->sum('total') }}€</td>
  								</tr>
  							</tfoot>
  							@endif

  						</table>


@if(Auth::user() && Auth::user()->isAdmin())
  <a href="{{ route('orders.edit', $order->id) }}" class="btn btn-primary">Edit</a>

{!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete', 'class' => 'btn-group'])  !!}
{!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
{!! Form::close() !!}

@endif


@endsection
