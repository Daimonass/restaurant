@extends('layouts.app')

@section('content')

<table class="table">

  <thead>
    <tr>
      <th>Uzsakymo pavadinimas</th>
      <th>Vardas</th>
      <th>Kaina</th>
      <th>Email</th>
      <th>Stalelio pavadinimas</th>
      <th>Reservation date</th>
      <th>Rezervacijos laikas</th>
      <th>PVM</th>
      <th>Suma be PVM</th>
    </tr>
  </thead>
<tbody>
  @if(isset($orders) && count($orders) > 0)
    @foreach ($orders as $order)

      <tr class="{{ $order->getReservationClass() }}">

        <td><h3><a href="{{ route('orders.show', $order->id) }}"> Uzsakymas {{$order->name}}</a></h3></td>
        <td>{{$order->name}}</td>
        <td>{{$order->total}}</td>
        <td>{{$order->email}}</td>
        <td>{{$order->table->title }}</td>
        <td>{{$order->reservation_date}}</td>
        <td>{{$order->reservation_time}}</td>
      <td>PVM {{ $order->getVat() }} € </td>
      <td>Suma be PVM: {{ $order->getWithoutVat() }} €</td>
      </tr>
    @endforeach
  @endif
</tbody>
</table>
@endsection
