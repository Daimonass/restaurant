@extends('layouts.app')

@section('content')

<h1>{{ $contact->title}}
  @if(\Auth::check() && \Auth::user()->type == 'admin')
  <a href="{{ route('contact.edit', $contact->id) }}" class="btn btn-primary pull-right">Edit</a>
  @endif
</h1>

<div class="row">
  <div class="col-md-6">
    <p>{{ $contact->address}}</p>
    <br>
    <p>Email: {{ $contact->email}}</p>
    <p>Phone: {{ $contact->phone}}</p>
  </div>
  <div class="col-md-6">
    <table class="table">
    {!! $contact->hours !!}
    </table>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <iframe src="{{$contact->getMapSrc()}}" frameborder="0" style="border:0; width:100%; height:450px;" allowfullscreen></iframe>

  </div>
</div>






@endsection
