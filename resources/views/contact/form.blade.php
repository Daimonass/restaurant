@extends('layouts.app')

@section('content')

  @if(isset($contact))
    {!! Form::model($contact, ['route' => ['contact.update', $contact->id], 'method' => 'put'])  !!}
  @else
    {!! Form::open(['route' => ['contact.store'], 'method' => 'post'])  !!}
  @endif

  <div class="form-group">
    {!! Form::label('title', 'Restaurant Title'); !!}
  	{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'TITLE']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('address', 'Address'); !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('email', 'Emailas'); !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Emailas']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('phone', 'Phone'); !!}
    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('hours', 'Working hour'); !!}
    {!! Form::textarea('hours', null, ['class' => 'form-control', 'placeholder' => 'Hour']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('map', 'Map address'); !!}
    {!! Form::text('map', null, ['class' => 'form-control', 'placeholder' => 'url..']) !!}
  </div>




  {!! Form::submit('Save',['class' => 'btn btn-primary']) !!}


  {{ Form::close() }}
@endsection
