@extends('layouts.app')

@section('content')



<h1><span>Name: {{ $user->name }}</span>
  <td><a href="{{ route('users.edit', ['id' => $user->id]) }}" class="btn btn-warning pull-right">Edit</a></td>
</h1>
<br>
<div class="row">
  <div class="col-md-6">
    <span>Surname: {{ $user->surname }}</span><br>
    <span>Email: {{ $user->email }}</span><br>
    <span>Date: {{ $user->date }}</span><br>
    <span>Phone: {{ $user->phone }}</span><br>
    <span>type: {{ $user->type }}</span><br>
  </div>
  <div class="col-md-6">
    <span>Address: {{ $user->address }}</span><br>
    <span>City: {{ $user->city }}</span><br>
    <span>zip: {{ $user->zip }}</span><br>
    <span>country: {{ $user->getCoutryName() }}</span><br>
  </div>
</div>
<ul class="nav nav-tabs">
  <li class="active"><a href="#">Orders</a></li>
</ul>
<div class="table-responsive">
<table id="table-with-sorting" class="table">
  <thead>
    <tr>
      <th>Order id</th>
      <th>Table name  </th>
      <th>Phone</th>
      <th>Total price</th>
      <th>Dishes count</th>
      <th>Email</th>
      <th>Reservation date</th>
      <th>Reservation time</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($user->orders as $order)

    <tr class="{{ $order->getReservationClass() }}">
      <td> {{ $order->id }} </td>
      <td> {{ $order->table->title }} </td>
      <td> {{ $order->contact_phone }} </td>
       <td>{{ $order->total }}€</td>
      <td>{{ $order->order_lines->sum('quantity') }}</td>
      <td> {{ $order->email }} </td>
      <td> {{ $order->reservation_date }} </td>
      <td> {{ $order->reservation_time }} </td>
      <td>
          {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete'])!!}
              <a href="{{ route('orders.show', $order) }}" class="btn btn-default"><span class="glyphicon glyphicon-eye-open"></span> Peržiūra</a>
              {!! Form::hidden('redirect_url', route('users.show', $user)) !!}
              {!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
          {!! Form::close() !!}
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
</div>

{{--
@if(Auth::user() && Auth::user()->isAdmin())


  {!! Form::open(['route' => ['table.destroy', $table->id], 'method' => 'delete', 'class' => 'pull-right'])  !!}
  {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
  {!! Form::close() !!}

@endif --}}



  @endsection
