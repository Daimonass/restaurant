@extends('layouts.app')

@section('content')




  <h1>{{ $dish->title}}</h1>


  <img src="{{ asset('/storage/' . $dish->photo) }}" alt="{{ $dish->title }}" class="img-responsive">

  <p>{{ $dish->description}}</p>


  <ul>
  	<li>Kaina: {{ $dish->price }}€ </li>
  </ul>


  <br>
@if(Auth::user() && Auth::user()->isAdmin())
  <a href="{{ route('dishes.edit', $dish->id) }}" class="btn btn-primary">Edit</a>


{!! Form::open(['route' => ['dishes.destroy', $dish->id], 'method' => 'delete', 'class' => 'btn-group'])  !!}
{!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
{!! Form::close() !!}

@endif






@endsection
