@extends('layouts.app')

@section('content')

@if(isset($dish))
  {!! Form::model($dish, ['route' => ['dishes.update', $dish->id], 'method' => 'put', 'files' => true])  !!}
@else
  {!! Form::open(['route' => ['dishes.store'], 'method' => 'post', 'files' => true])  !!}
@endif


<div class="form-group">
  {!! Form::label('title', 'Title'); !!}
	{!!  Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'TITLE']) !!}
</div>

<div class="form-group">
	{!! Form::file('photo', null) !!}
</div>

{{-- <div class="form-group">
  {!! Form::label('photo', 'Photo'); !!}
	{!!  Form::text('photo', null, ['class' => 'form-control']) !!}
</div> --}}

<div class="form-group">
	{!! Form::label('price', 'Price'); !!}
	{!!  Form::text('price', null, ['class' =>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('netto_price', 'Netto price'); !!}
	{!!  Form::text('netto_price', null, ['class' =>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('quantity', 'Quantity'); !!}
	{!!  Form::number('quantity', null, ['class' =>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('desciption', 'Desciption'); !!}
	{!!  Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Desciption']) !!}
</div>

{!! Form::submit('Save',['class' => 'btn btn-primary']) !!}


{{ Form::close() }}

@endsection
