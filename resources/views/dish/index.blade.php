@extends('layouts.app')

@section('content')

  @foreach ($dishes->chunk(3) as $chunk)
    <div class="row">
      @foreach ($chunk as $dish)
        <div class="col-md-4">
          <div class="thumbnail">

            <a href="{{ route('dishes.show', $dish->id) }}"><img src="{{ asset('/storage/' . $dish->photo) }}" alt="{{ $dish->title }}" class="img-responsive"></a>
            <div class="caption">
              <h3>{{ $dish->title }}</h3>
              <p>{{ $dish->description }}</p>
              <p>
                Kaina: {{ $dish->price }}€
                Net kaina: {{ $dish->netto_price }}€
                Kiekis: {{ $dish->quantity }}vnt.
              </div>
              <a href="#" class="btn btn-warning add-to-cart" data-id="{{$dish->id}}">Add to cart</a>
            </div>
          </div>
        @endforeach
      </div>
    @endforeach

  @endsection
