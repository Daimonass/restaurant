@extends('layouts.app')

@section('content')

  @if(isset($table))
    {!! Form::model($table, ['route' => ['table.update', $table->id], 'method' => 'put', 'files' => true])  !!}
  @else
    {!! Form::open(['route' => ['table.store'], 'method' => 'post', 'files' => true])  !!}
  @endif


  <div class="form-group">
    {!! Form::label('title', 'Name'); !!}
  	{!!  Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
  </div>

  <div class="form-group">
  	{!! Form::file('photo', null) !!}
  </div>

  <div class="form-group">
    {!! Form::label('description', 'Name'); !!}
    {!!  Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('min', 'minumum zmoniu'); !!}
    {!!  Form::text('min', null, ['class' => 'form-control', 'placeholder' => 'Minimum']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('max', 'miximum zmoniu'); !!}
    {!!  Form::text('max', null, ['class' => 'form-control', 'placeholder' => 'Miximum']) !!}
  </div>






  {!! Form::submit('Save',['class' => 'btn btn-primary']) !!}


  {{ Form::close() }}


@endsection
