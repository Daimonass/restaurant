@extends('layouts.app')

@section('content')


  <table class="table table-bordered text-center">

    <thead>
      <tr>
        <th>Nuotrauka</th>
        <th>Numeris</th>
        <th>Minimalus sedimu vietu skaicius</th>
        <th>Maxsimalus sedimu vietu skaicius</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($tables as $table)
        <tr>
          <td><img src="{{ asset('/storage/' . $table->photo) }}" alt="" class="img-responsive"></td>
          <td>{{ $table->id }}</td>
          <td>{{ $table->min }}</td>
          <td>{{ $table->max }}</td>
          <td><a href="{{ route('table.show', ['id' => $table->id]) }}" class="btn btn-warning">Perziureti</a></td>
        </tr>
      @endforeach
    </tbody>
  </table>








  @endsection
