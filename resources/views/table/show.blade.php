@extends('layouts.app')

@section('content')








  <h1>{{ $table->title}}</h1>


  <img src="{{ asset('/storage/' . $table->photo) }}" alt="{{ $table->title}}" class="img-responsive">

  <p>{{ $table->description}}</p>


  <ul>
  	<li>Minimum zmoniu: {{ $table->min }}</li>
  	<li>Maximum zmoniu: {{ $table->max }}</li>
  </ul>


  <br>
@if(Auth::user() && Auth::user()->isAdmin())
  <td><a href="{{ route('table.edit', ['id' => $table->id]) }}" class="btn btn-warning">Edit</a></td>

  {!! Form::open(['route' => ['table.destroy', $table->id], 'method' => 'delete', 'class' => 'pull-right'])  !!}
  {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
  {!! Form::close() !!}

@endif















  @endsection
