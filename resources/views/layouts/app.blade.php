  <!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/css/bootstrap-formhelpers.min.css" />

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'cartAddRoute' => route('cart.add'),
            'cartClearRoute' => route('cart.clear'),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">Restaurant
                        {{-- {{ config('app.name', 'Laravel') }} --}}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                      <li><a href="{{ url('contact') }}">Contact</a></li>
                      <li><a  href="{{ url('dishes') }}">Dish Menu</a></li>
                      <li><a  href="{{ url('orders') }}">Orders</a></li>
                      <li><a  href="{{ url('table') }}">Table</a></li>

                      {{-- <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
                        <span class="caret"></span></button>

                        <ul class="dropdown-menu">
                          @foreach ($dishes as $dish)
                            <li><a href="#">{{ $dish->title }}</a></li>
                          @endforeach
                        </ul>

                    </div> --}}
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">

                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                          @if(Auth::user() && Auth::user()->isAdmin())
                          <li><a href="{{ route('users.index') }}">User</a></li>
                          @endif
                          <li><a href="{{ route('cart.checkout') }}">Checkout</a></li>

                  <li class="dropdown">

                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                          Total: <span id="cart-total">{{ session('cart.total') ?: '0'}}</span> &euro;<span class="caret"></span>
                      </a>

                      <ul id="cart-items" class="dropdown-menu" role="menu">
                    @if (session('cart.items') && count(session('cart.items')) >0)
                    @foreach (session('cart.items') as $item)
                        <li>
                            <a href="#">Total: <span id="cart-total">{{ $item['title']}} x {{ $item['quantity']}} | <strong>{{$item['total']}}</strong></span>€</a>
                          </li>
                    @endforeach
                    @endif
                      </ul>
                    </li>

                    <li><a href="#" id="clear-cart">X</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>

                                          <a href="{{ route('profile') }}">Profile</a>

                                      @if(Auth::user() && Auth::user()->isAdmin())
                                        <a href="{{ route('dishes.create') }}">Create Dishes</a>
                                        <a href="{{ route('table.create') }}">Create Tables</a>
                                        <a href="{{ route('contact.create') }}">Create Contact</a>
                                      @endif

                                      <a href="{{ route('logout') }}"
                                          onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                          Logout
                                      </a>



                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
<div class="container">

  @if (session('message'))
    <div class="alert alert-{{ session('message')['type'] }}">
      {{ session('message')['text'] }}
    </div>
  @endif

  @yield('content')
</div>

    </div>

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

</body>
</html>
