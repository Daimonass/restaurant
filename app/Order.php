<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\OrderLine;
use Carbon\Carbon;

class Order extends Model
{
  protected $fillable = [
    'name',
    'email',
    'total',
    'date',
    'user_id',
    'contact_phone',
    'number_of_persons',
    'date',
    'reservation_time',
    'reservation_date',
    'table_id',
  ];

  public function order_lines()
  {
    return $this->hasMany('App\OrderLine');
  }

  public function user(){
    return $this->belongsTo('App\User');
  }
  public function table(){
    return $this->belongsTo('App\Table');
  }
public function getReservationClass()
{
  // Sviesaforas rodomas tik tada kai ziuri administratorius
  if (\Auth::check() && \Auth::user()->isAdmin()) {
    $date = Carbon::parse($this->reservation_date);

    if ($date->isToday()) {
      // yra siandien
      return 'danger';

    }else if ($date->isTomorrow()){
      //rytoj
      return 'warning';

    }else if ($date->isPast()) {
      //vakar
      return ' success';

    }else {
      //Ateitis po rytojaus
      return 'default';
    }
  }

  return '';
}

    public function getWithoutVat()
    {
        return \App\Price::getWithoutVat($this->total);
    }

    public function getVat()
    {
        return $this->total - $this->getWithoutVat();
    }

}
