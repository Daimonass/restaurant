<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'date', 'phone', 'email', 'password', 'address', 'city', 'zip', 'country',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders(){
      return $this->hasMany('App\Order');
    }

    public function isAdmin()
    {
      return $this->type == 'admin';
    }

    public function getCoutryName()
    {
      $countries = [
        "LT" => 'Lietuva',
      ];

      return isset($countries[$this->country]) ? $countries[$this->country] : $this->country;
    }
}
