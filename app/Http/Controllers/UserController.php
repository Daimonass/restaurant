<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;

class UserController extends Controller
{

  public function __construct(){
    $this->middleware('auth.Admin');
  }


  public function profile()
  {
      $user = \Auth::user();
      $title = 'Update profile';
      return view('auth.register', compact('user', 'title'));
  }

  public function update(Request $request)
  {
      $user = \Auth::user();

      $user->name = $request->name;
      $user->surname = $request->surname;
      $user->date = $request->date;
      $user->phone = $request->phone;
      $user->city = $request->city;
      $user->zip = $request->zip;
      $user->country = $request->country;
      $user->save();

      return redirect()->route('dishes.index')->with('message', [
          'text' => 'Profile updated!',
          'type' => 'success'
      ]);
  }

  public function index()
  {
    $users = User::all();
    return view('user.index' , compact('users'));
  }

  public function show(User $user)
  {
    $user = User::find($user);
    return view('user.show', compact('user'));
  }

  public function edit(User $user){
    //
  }

  public function destroy(User $user)
  {
    $user->delete();

    return redirect()->route('users.index');
  }
}
