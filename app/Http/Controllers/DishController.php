<?php

namespace App\Http\Controllers;

use App\Dish;
use Illuminate\Http\Request;

class DishController extends Controller
{

  public function __construct(){
    $this->middleware('auth.Admin')->except(['index', 'show']);
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes = Dish::all();
        return view('dish.index' , compact('dishes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dish.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->hasFile('photo')) {
          $file = $request->file('photo');
          $file->store('public');
          $request->photo = $file->hashName();
        }

        Dish::create([
          'title' => $request->get('title'),
          'photo' => $request->photo,
          'price' => $request->get('price'),
          'netto_price' => $request->get('netto_price'),
          'quantity' => $request->get('quantity'),
          'description' => $request->get('description'),
        ]);

        return redirect()->route('dishes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $dish = Dish::find($id);
      return view('dish.show', compact('dish'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dish = Dish::find($id);
        return view('dish.form', compact('dish'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Dish $dish)
    {

      if ($request->hasFile('photo')) {
          $file = $request->file('photo');
          $file->store('public');
          $request->photo = $file->hashName();
        }

        $dish->update([
          'title' => $request->get('title'),
          'photo' => $request->photo,
          'price' => $request->get('price'),
          'netto_price'=> $request->get('netto_price'),
          'quantity' => $request->get('quantity'),
          'description' => $request->get('description'),
        ]);
        return redirect()->route('dishes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dish $dish)
    {
        $dish->delete();

        return redirect()->route('dishes.index');
    }
}
