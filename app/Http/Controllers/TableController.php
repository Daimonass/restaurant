<?php

namespace App\Http\Controllers;

use App\Table;
use App\Order;
use Illuminate\Http\Request;

class TableController extends Controller
{
  public function __construct(){
    $this->middleware('auth.Admin')->except(['index', 'show']);
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tables = Table::all();
        return view('table.index' , compact('tables'));
        // return view('order.checkout' , compact('tables'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('table.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->hasFile('photo')) {
          $file = $request->file('photo');
          $file->store('public');
          $request->photo = $file->hashName();
        }

        Table::create([
          'title' => $request->get('title'),
          'photo' => $request->photo,
          'min' => $request->get('min'),
          'max' => $request->get('max'),
          'description' => $request->get('description'),
        ]);

        return redirect()->route('table.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $table = Table::find($id);
      return view('table.show', compact('table'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $table = Table::find($id);
      return view('table.form', compact('table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Table $table)
    {
      $table->title =  $request->get('title');
      $table->min = $request->get('min');
      $table->max = $request->get('max');


      if($request->get('description')){
        $table->description = $request->get('description');
      }


      if ($request->hasFile('photo')) {
          $file = $request->file('photo');
          $file->store('public');
          $table->photo = $file->hashName();
        }

        $table->save();
        return redirect()->route('table.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function destroy(Table $table)
    {
      $table->delete();

      return redirect()->route('table.index');
    }
}
