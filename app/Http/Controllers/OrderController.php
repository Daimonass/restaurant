<?php

namespace App\Http\Controllers;

use App\Order;
use App\Dish;
use App\Table;
use App\OrderLine;
use Illuminate\Http\Request;
use App\Price;

class OrderController extends Controller
{

  public function __construct(){
    //auth.admin gali viska, iskyrus visi kiti except([...])
    $this->middleware('auth.Admin')->except(['index', 'show','addToCart', 'clearCart', 'deleteLine', 'checkout', 'store']);
    //auth gali tai ka leido auth.Admin, o non-register gali tai kas yra addToCart
    $this->middleware('auth')->except(['addToCart', 'clearCart', 'deleteLine', 'checkout']);
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(\Auth::user()->isAdmin()){
        $orders = Order::orderBy('date', 'desc')->get();
      }else{
        // $orders = Order::where('user_id', \Auth::user()->id)->get();
        $orders = \Auth::user()->orders()->orderBy('date', 'desc')->get();
      }
      // dump($orders);
      // return view('order.index' , ['orders' => $orders]);
        return view('order.index' , compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (session('cart.total')) {
          return view('order.form');
        }else{
          return redirect()->route('dishes.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $order = Order::create([
      //   'name' => $request->name,
      //   'email' => $request->email,
      //   'total' => session('cart.total'),
      //   'date' => \Carbon\Carbon::now(),
      //   'user_id'=>\Auth::user()->id,
      // ]);

      $order = Order::create([
        'name' => session('cart.reservation_info')['name'],
        'email' => session('cart.reservation_info')['email'],
        'contact_phone' => session('cart.reservation_info')['contact_phone'],
        'number_of_persons' => session('cart.reservation_info')['number_of_persons'],
        'table_id' => session('cart.reservation_info')['table_id'],
        'date' => \Carbon\Carbon::now(),
        'reservation_date' => session('cart.reservation_info')['reservation_date'],
        'reservation_time' => session('cart.reservation_info')['reservation_time'],
        'total' => session('cart.total'),
        'user_id'=>\Auth::user()->id,
      ]);

      foreach (session('cart.items') as $item) {
        OrderLine::create([
          'order_id' => $order->id,
          'dish_id' => $item['id'],
          'quantity'=> $item['quantity'],
          'total' => $item['total']
        ]);
      }

      $this->clearCart();

      return redirect()->route('contact.index')->with('message', [
        'text' => 'Uzsakymas ivykdytas',
        'type' => 'success'
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $order = Order::find($id);
      return view('order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $table = Table::find(session('cart.reservation_info.table_id'));
      $dishes = Dish::all();
      $order = Order::find($id);
      return view('order.form', compact('order','table','dishes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

              'name' => 'required|max:255',
              'email' => 'required|email|max:255',
              'number_of_persons' => 'required|numeric|between:0,15',
              'reservation_date' => 'required|max:255',
              'reservation_time' => 'required|max:255',

           ]);

      Order::find($id)->update($request->all());
      return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order, Request $request)
    {
      $order->order_lines()->delete();
      $order->delete();

            if ($request->has('redirect_url')) {
            return redirect()->to($request->redirect_url);

             }

      return redirect()->route('orders.index');
    }

    public function destroyLine($id)
    {
        $line = OrderLine::findOrFail($id);

        /**
         * pakoreguoja bendrą orderio sumą
         */
        $line->order->total -= $line->total;
        $line->order->save();

        $line->delete();

        return redirect()->route('orders.edit', $line->order->id);
    }

    public function addToCart(Request $request){
            // session(['cart.items'=> [] ]);
            $dish_id = $request->id;
            $dish = Dish::find($dish_id);
            $product = [
            'id' => $dish->id,
            'photo' => $dish->photo,
            'title' => $dish->title,
            'quantity' => 1,
            'netto_price' => $dish->netto_price,
            'price' => $dish->price,
            'total' =>$dish->price,
            'without_vat' =>$dish->getWithoutVat(),
            'vat' =>$dish->getVat()
            ];
            $items = session('cart.items');
            $existing = false;

            if ($items && count($items) > 0) {

                foreach($items as $index => $item) {
                    if($item['id'] == $dish_id) {
                        $items[$index]['quantity'] = $item['quantity'] + $product['quantity'];
                        $items[$index]['total'] = $items[$index]['quantity'] * $dish->FormattedPrice;

                        $items[$index]['without_vat'] = $items[$index]['quantity'] * $dish->getWithoutVat();
                        $items[$index]['vat'] = $items[$index]['quantity'] * $dish->getVat();
                        $existing = true;
                    }

                }
            }
            if(! $existing){
                session()->push('cart.items', $product);
            } else {
                session(['cart.items'=> $items]);
            }

            $this->calculateCartTotals();

            return session('cart');
        }

    public function clearCart(){
      session([
        'cart.items' =>[],
        'cart.total_without_vat' => 0,
        'cart.total_vat' => 0,
        'cart.total' => 0,
      ]);
    }

    public function deleteLine(Request $request) {
            $id = $request->id;
            $items = session('cart.items');
            $total = session('cart.total');

            foreach($items as $index =>$item){
                if($item['id'] == $id){
                    $total -= $item['total'];
                    unset($items[$index]);
                    break;
                }
            }
            session([
                    'cart.items' => $items
            ]);

            $this->calculateCartTotals();

            return redirect()->route('cart.checkout');
        }

    public function checkout() {
            $tables = Table::all()->pluck('title', 'id');

            return view('order.checkout', compact('tables'));
    }

    private function calculateCartTotals() {
            $items = session('cart.items');
            $grand_total = 0;

            foreach($items as $item) {
                $grand_total = $grand_total + $item['total'];
            }

            $withoutVat = Price::getWithoutVat($grand_total);
            $vat = Price::getVat($grand_total);

            session([
                'cart.total_without_vat' => $withoutVat,
                'cart.total_vat' => $vat,
                'cart.total' => $grand_total
            ]);
        }

    private function addDishToOrder(Request $request, Dish $dish)
    {

    }
}
