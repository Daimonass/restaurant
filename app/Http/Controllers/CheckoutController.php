<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Table;
use Illuminate\Support\Facades\Validator;

class CheckoutController extends Controller
{

  public function __construct(){
    $this->middleware('auth');
  }

  public function store(Request $request)
  {
    $this->validate($request, [

          'name' => 'required|max:255',
          'table_id' => 'required|exists:tables,id',
          'email' => 'required|email|max:255',
          'contact_phone' => 'required|numeric',
          'number_of_persons' => 'required|numeric|between:0,15',
          'reservation_date' => 'required|max:255',
          'reservation_time' => 'required|max:255',

       ]);

    $reservationInfo = [
      'name' => $request->name,
      'email' => $request->email,
      'contact_phone' => $request->contact_phone,
      'number_of_persons' => $request->number_of_persons,
      'table_id' => $request->table_id,
      'reservation_date' => $request->reservation_date,
      'reservation_time' => $request->reservation_time,
      'total' => session('total'),
    ];

     session()->put('cart.reservation_info', $reservationInfo);
    //  dump(session()->all());
     return redirect()->route('checkout.confirm');
  }

  public function confirm()
  {
      $table = Table::find(session('cart.reservation_info.table_id'));
      return view('order.confirm', compact('table'));
  }
}
