<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
  public function __construct(){
    $this->middleware('auth.Admin')->except('index');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $contact = Contact::first();
      return view('contact.show' , compact('contact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      Contact::create([
        'title' => $request->get('title'),
        'address' => $request->get('address'),
        'email' => $request->get('email'),
        'phone' => $request->get('phone'),
        'hours' => $request->get('hours'),
        'map' => $request->get('map'),
      ]);

      return redirect()->route('contact.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
      $contact = Contact::find($contact);
      return view('contact.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
      $contact = Contact::find($contact);
      return view('contact.form', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
      $contact->update([
        'title' => $request->get('title'),
        'address' => $request->get('address'),
        'email' => $request->get('email'),
        'phone' => $request->get('phone'),
        'hours' => $request->get('hours'),
        'map' => $request->get('map'),
      ]);

      return redirect()->route('contact.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
      $contact->delete();

      return redirect()->route('contact.show');
    }
}
