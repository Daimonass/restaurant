<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'DishController@index');

Route::get('/home', 'HomeController@index');
Route::resource('table', 'TableController');
Route::get('saulius', 'HomeController@saulius');

Route::resource('dishes', 'DishController');

Route::resource('orders', 'OrderController');

Route::get('checkout', 'OrderController@checkout')->name('cart.checkout');

Route::post('cart', 'OrderController@addToCart')->name('cart.add');
Route::delete('cart', 'OrderController@clearCart')->name('cart.clear');
Route::get('cart/delete/{id}' ,'OrderController@deleteLine')->name('cart.delete_line');
Route::post('cart/reservation', 'CheckoutController@store')->name('checkout.store');
Route::get('cart/confirm', 'CheckoutController@confirm')->name('checkout.confirm');

Route::get('/profile', 'UserController@user')->name('profile.edit');
Route::get('/profile', 'UserController@profile')->name('profile')->middleware('auth');
Route::put('/profile', 'UserController@update')->name('profile.update')->middleware('auth');

Route::resource('contact', 'ContactController');

Route::delete('orders/line/{id}/destroy', 'OrderController@destroyLine')->name('orders.line_destroy');

Route::put('add/dish', 'OrderController@addDishToOrder')->name('add.dist_to_order');

//user
Route::resource('users',  'UserController');
